const fetch = require("node-fetch")
const fs = require('fs')
var stdio = require('stdio')

const fetchIndexFile = (base,id) => {
    return fetch(`${base}${id}/`)
        .then(function(response) {
            return response.text();
        })
        .then(function(content) {
            let collection = {}
            collection.cid = content.match(/'collectionId':'(.*?)'/)[1]
            collection.cuk = content.match(/'collectionUrlKey':'(.*?)'/)[1]
            collection.fk = ""
            collection.gs = "highlights"
            collection.query = Object.entries(collection).map(([k,v]) => `${k}=${v}`).join("&")
            return collection;
        })
}
const fetchPage = (base, collection, page) => {

    return fetch(`${base}client/loadphotos/?${collection.query}&page=${page}`,{
        headers:{"x-requested-with":"XMLHttpRequest"}
    }).then(response => response.json()).then(content => {
        if(content.status == "success"){
            return JSON.parse(content.content)
        }else{
            return []
        }
    })
}
const fetchPages = async (base, collection) => {
    let photos = []
    let count = 1;
    let i = 1;
    while(count != 0){
        console.log("Load index file",i)
        let content = await fetchPage(base, collection, i++);
        photos = photos.concat(content.map(x => x.pathXxlarge))
        count = content.length
    }
    return photos
}

const download = (url, path, name) => {
    return fetch(url).then(x => x.arrayBuffer()).then(x => {
        fs.writeFile(`${path}/${name}`, Buffer.from(x), () => {});
        console.log("Downloaded ", name)
    })
}


var ops = stdio.getopt({
    'host': {key: 'h', description: 'The Pixieset Host', mandatory: true, args: 1},
    'collection': {key: 'c', description: 'The Pixieset Collection Name', mandatory: true, args: 1},
    'output': {key: 'o', description: 'The ouput folder', mandatory: true, args: 1},
    'max': {key: 'm', description: 'The maximum number of photos to be downloaded', default: 1000, args: 1}
});


fetchIndexFile(`http://${ops.host}/`,ops.collection).then((collection) => {
    fetchPages(`http://${ops.host}/`, collection).then(photos => {
        photos.slice(0,ops.max).forEach((photo, i) => download(`https:${photo}`,"photos",`image-${(1e5+i+"").slice(-3)}.jpg`))
    })
})