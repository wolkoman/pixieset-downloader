# Pixiset Downloader

## Purpose
Pixieset is a web software for showing professional photo galleries. They are trying to prevent the download of the photos showcased but obviously fail to do so, since the photos must be downloaded to your PC to be shown. This program downloads all the photos in the highest publicly available resolution (xxlarge). If you have access to the download code from the photographer, use it (original files are much higher in resolution and overall image quality).

## Usage
I haven't made any executables, so one must run the program directly from source. Here is the way to go:

 - You need to download  `NodeJS`from [the offical site](https://nodejs.org/en/download/) 
 - Download  all the files from this repository to a folder.
 - Open a terminal in that folder
	 - Download all dependencies with the the command `npm i`
	 - Start the program using the command `node index.js -h HOST -c COLLECTION -o OUTPUT_FOLDER [-m DOWNLOAD_LIMIT]`

## Legal Situation
Please inform yourself on the legal situation in your country. It may be legal to download copyrighted material for your private use.

## License
MIT License

